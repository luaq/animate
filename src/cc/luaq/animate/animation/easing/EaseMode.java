package cc.luaq.animate.animation.easing;

/**
 * AniMate - Java was created and distributed by Luaq.
 * Please do not redistribute as one's own regardless of the
 * situation, this project (code and all), belongs to Luaq.
 */
public enum EaseMode {

    EMPTY,
    EASE_IN,
    EASE_OUT,
    EASE_IN_OUT

}

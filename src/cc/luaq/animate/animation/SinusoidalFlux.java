package cc.luaq.animate.animation;

import cc.luaq.animate.animation.easing.EaseMode;
import cc.luaq.animate.animation.interfaces.IRenewableFlux;
import cc.luaq.animate.exceptions.AniMateException;

/**
 * AniMate - Java was created and distributed by Luaq.
 * Please do not redistribute as one's own regardless of the
 * situation, this project (code and all), belongs to Luaq.
 */
public class SinusoidalFlux implements IRenewableFlux {

    private final long lengthMillis;
    private final float startValue;
    private final float endValue;
    private long startTime;
    private EaseMode easeMode;

    public SinusoidalFlux(long lengthMillis, float startValue, float endValue) {
        this(Long.MAX_VALUE, lengthMillis, startValue, endValue, EaseMode.EMPTY);
    }

    public SinusoidalFlux(long lengthMillis, float startValue, float endValue, EaseMode easeMode) {
        this(Long.MAX_VALUE, lengthMillis, startValue, endValue, easeMode);
    }

    public SinusoidalFlux(long startTime, long lengthMillis, float startValue, float endValue) {
        this(startTime, lengthMillis, startValue, endValue, EaseMode.EMPTY);
    }

    public SinusoidalFlux(long startTime, long lengthMillis, float startValue, float endValue, EaseMode easeMode) {
        this.startTime = startTime;
        this.lengthMillis = lengthMillis;
        this.startValue = startValue;
        this.endValue = endValue;
        this.easeMode = easeMode;
    }

    @Override
    public float calculateValue() {
        if (System.currentTimeMillis() < startTime) return startValue;
        float returnValue = startValue;
        float v = (System.currentTimeMillis() - startTime);
        float abs = Math.abs(startValue - endValue);
        if (easeMode == EaseMode.EMPTY || easeMode == EaseMode.EASE_IN) {
            returnValue = -abs * (float) Math.cos(v / (float) lengthMillis * (Math.PI / 2)) + abs + startValue;
        } else if (easeMode == EaseMode.EASE_OUT) {
            returnValue = abs * (float) Math.sin(v / (float) lengthMillis * (Math.PI / 2)) + startValue;
        } else if (easeMode == EaseMode.EASE_IN_OUT) {
            returnValue = -abs / 2 * ((float) Math.cos(Math.PI * v / (float) lengthMillis) - 1) + startValue;
        }
        if (returnValue >= endValue) {
            return endValue;
        }
        return returnValue;
    }

    @Override
    public void applyEaseMode(EaseMode mode) throws AniMateException {
        this.easeMode = mode;
    }

    @Override
    public EaseMode getEaseMode() {
        return easeMode;
    }

    @Override
    public float getStartValue() {
        return startValue;
    }

    @Override
    public float getEndValue() {
        return endValue;
    }

    @Override
    public long getStartTime() {
        return startTime;
    }

    @Override
    public long getDuration() {
        return lengthMillis;
    }

    @Override
    public long getEndTime() {
        return startTime + lengthMillis;
    }

    @Override
    public void startAnimation() {
        this.startTime = System.currentTimeMillis();
    }

    @Override
    public void resetAnimation() {
        this.startTime = Long.MAX_VALUE;
    }

}

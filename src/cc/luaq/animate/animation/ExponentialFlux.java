package cc.luaq.animate.animation;

import cc.luaq.animate.animation.easing.EaseMode;
import cc.luaq.animate.animation.interfaces.IFlux;
import cc.luaq.animate.animation.interfaces.IRenewableFlux;
import cc.luaq.animate.exceptions.AniMateException;

/**
 * AniMate - Java was created and distributed by Luaq.
 * Please do not redistribute as one's own regardless of the
 * situation, this project (code and all), belongs to Luaq.
 */
public class ExponentialFlux implements IRenewableFlux {

    private long startTime;
    private final long lengthMillis;
    private final float startValue;
    private final float endValue;
    private EaseMode easeMode;

    public ExponentialFlux(long lengthMillis, float startValue, float endValue) {
        this(Long.MAX_VALUE, lengthMillis, startValue, endValue, EaseMode.EMPTY);
    }

    public ExponentialFlux(long lengthMillis, float startValue, float endValue, EaseMode easeMode) {
        this(Long.MAX_VALUE, lengthMillis, startValue, endValue, easeMode);
    }

    public ExponentialFlux(long startTime, long lengthMillis, float startValue, float endValue) {
        this(startTime, lengthMillis, startValue, endValue, EaseMode.EMPTY);
    }

    public ExponentialFlux(long startTime, long lengthMillis, float startValue, float endValue, EaseMode easeMode) {
        this.startTime = startTime;
        this.lengthMillis = lengthMillis;
        this.startValue = startValue;
        this.endValue = endValue;
        this.easeMode = easeMode;
    }

    @Override
    public float calculateValue() {
        if (System.currentTimeMillis() < startTime) return startValue;
        float returnValue = startValue;
        float abs = Math.abs(startValue - endValue);
        float v = (System.currentTimeMillis() - startTime);
        if (easeMode == EaseMode.EMPTY || easeMode == EaseMode.EASE_IN) {
            returnValue = abs * (float) Math.pow(2, 10 * (v / (float) lengthMillis - 1)) + startValue;
        } else if (easeMode == EaseMode.EASE_OUT) {
            returnValue = abs * (float) -Math.pow(2, -10 * (v / (float) lengthMillis + 1)) + startValue;
        } else if (easeMode == EaseMode.EASE_IN_OUT) {
            v /= (float) lengthMillis / 2;
            if (v < 1) returnValue = abs / 2 * (float) Math.pow(2, 10 * (v - 1)) + startValue;
            else {
                --v;
                returnValue = abs / 2 * ((float) -Math.pow(2, -10 * v) + 2) + startValue;
            }
        }
        if (returnValue >= endValue) {
            return endValue;
        }
        return returnValue;
    }

    @Override
    public void applyEaseMode(EaseMode mode) throws AniMateException {
        this.easeMode = mode;
    }

    @Override
    public EaseMode getEaseMode() {
        return easeMode;
    }

    @Override
    public float getStartValue() {
        return startValue;
    }

    @Override
    public float getEndValue() {
        return endValue;
    }

    @Override
    public long getStartTime() {
        return startTime;
    }

    @Override
    public long getDuration() {
        return lengthMillis;
    }

    @Override
    public long getEndTime() {
        return startTime + lengthMillis;
    }

    @Override
    public void startAnimation() {
        this.startTime = System.currentTimeMillis();
    }

    @Override
    public void resetAnimation() {
        this.startTime = Long.MAX_VALUE;
    }

}

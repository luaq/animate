package cc.luaq.animate.animation.interfaces;

import cc.luaq.animate.animation.easing.EaseMode;
import cc.luaq.animate.exceptions.AniMateException;

/**
 * AniMate - Java was created and distributed by Luaq.
 * Please do not redistribute as one's own regardless of the
 * situation, this project (code and all), belongs to Luaq.
 */
public interface IFlux {

    float calculateValue();

    void applyEaseMode(EaseMode mode) throws AniMateException;

    EaseMode getEaseMode();

    float getStartValue();

    float getEndValue();

    long getStartTime();

    long getDuration();

    long getEndTime();

}

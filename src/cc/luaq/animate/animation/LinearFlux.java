package cc.luaq.animate.animation;

import cc.luaq.animate.animation.easing.EaseMode;
import cc.luaq.animate.animation.interfaces.IFlux;
import cc.luaq.animate.animation.interfaces.IRenewableFlux;
import cc.luaq.animate.exceptions.AniMateException;
import cc.luaq.animate.exceptions.EaseModeException;

/**
 * AniMate - Java was created and distributed by Luaq.
 * Please do not redistribute as one's own regardless of the
 * situation, this project (code and all), belongs to Luaq.
 */
public class LinearFlux implements IRenewableFlux {

    private long startTime;
    private final long lengthMillis;
    private final float startValue;
    private final float endValue;

    public LinearFlux(long lengthMillis, float startValue, float endValue) {
        this(Long.MAX_VALUE, lengthMillis, startValue, endValue);
    }

    public LinearFlux(long startTime, long lengthMillis, float startValue, float endValue) {
        this.startTime = startTime;
        this.lengthMillis = lengthMillis;
        this.startValue = startValue;
        this.endValue = endValue;
    }

    @Override
    public void applyEaseMode(EaseMode mode) throws AniMateException {
        throw new EaseModeException();
    }

    @Override
    public EaseMode getEaseMode() {
        return EaseMode.EMPTY;
    }

    @Override
    public float calculateValue() {
        if (System.currentTimeMillis() < startTime) return startValue;
        float v = Math.abs(startValue - endValue) * (System.currentTimeMillis() - startTime) / lengthMillis + startValue;
        if (v >= endValue) {
            return endValue;
        }
        return v;
    }

    @Override
    public long getStartTime() {
        return startTime;
    }

    @Override
    public long getDuration() {
        return lengthMillis;
    }

    @Override
    public long getEndTime() {
        return startTime + lengthMillis;
    }

    @Override
    public float getStartValue() {
        return startValue;
    }

    @Override
    public float getEndValue() {
        return endValue;
    }

    @Override
    public void startAnimation() {
        this.startTime = System.currentTimeMillis();
    }

    @Override
    public void resetAnimation() {
        this.startTime = Long.MAX_VALUE;
    }

}

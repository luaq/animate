package cc.luaq.animate.exceptions;

/**
 * AniMate - Java was created and distributed by Luaq.
 * Please do not redistribute as one's own regardless of the
 * situation, this project (code and all), belongs to Luaq.
 */
public class AniMateException extends Exception {

    public AniMateException(String message) {
        super(message);
    }

}

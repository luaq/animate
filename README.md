# AniMate
Easy tool to animate values in Java. The formulas used to calculate values exist thanks to http://www.gizma.com/easing/, would not have been possible without it.